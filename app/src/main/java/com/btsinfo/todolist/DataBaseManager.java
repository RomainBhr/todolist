package com.btsinfo.todolist;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;
import android.view.View;

import java.util.ArrayList;

public class DataBaseManager extends SQLiteOpenHelper {

    private static final String DATABASE_NAME="todolist.db";
    private static final int DATABASE_VERSION = 71;

    public DataBaseManager (MainActivity context){
        super(context,DATABASE_NAME,null,DATABASE_VERSION);
    }

    public DataBaseManager (Ajouter_Activit context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    public DataBaseManager(TacheActivity context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String strSqlE = "create table Prioriter(idP int primary key,nomP String ,imgP String, desP String,datedebut String,dateactuelle String, datefin String)";
        db.execSQL(strSqlE);

        String p1 = "insert into Prioriter values (1,'Romain', 'images/tache.png','Ceci est une bonne description a fin de voir ce qu il faut faire', '2021-01-15',date('now'), '2021-01-20' )";
        String p2 = "insert into Prioriter values (2,'Sortir boir un verre', 'images/bar.jpg','Ceci est une bonne description a fin de voir ce qu il faut faire','2021-01-17',date('now'),'2021-01-22')";
        String p3 = "insert into Prioriter values (3,'Ranger mon ordianteur', 'images/pc.jpg','Ceci est une bonne description a fin de voir ce qu il faut faire','2021-01-16',date('now'),'2021-01-19')";
        String p4 = "insert into Prioriter values (4,'Fin du couvre feu', 'images/feu.jpg','Ceci est une bonne description a fin de voir ce qu il faut faire','2021-01-14',date('now'),'2021-02-19')";
        String p5 = "insert into Prioriter values (5,'otmane', 'images/feu.jpg','Ceci est une bonne description a fin de voir ce qu il faut faire','2021-01-19',date('now'),'2021-01-17')";

        db.execSQL(p1);
        db.execSQL(p2);
        db.execSQL(p3);
        db.execSQL(p4);
        db.execSQL(p5);

        Log.i("DATABASE","onCreate invoqué");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        String strSqlE = "drop table Prioriter";
        db.execSQL(strSqlE);
        this.onCreate(db);
    }
    public ArrayList<prioriter> lecturePrioriter(){
        ArrayList<prioriter> equipes = new ArrayList<>();
        String sqlE = "select * from Prioriter ORDER BY datefin ";
        Cursor curseur = this.getReadableDatabase().rawQuery(sqlE, null);
        curseur.moveToFirst();
        while(!curseur.isAfterLast()){
            prioriter prioriter = new prioriter(curseur.getInt(0),curseur.getString(1),curseur.getString(2),curseur.getString(3),curseur.getString(4),curseur.getString(5),curseur.getString(6));
            equipes.add(prioriter);
            curseur.moveToNext();
        }
        curseur.close();
        return equipes;
    }
    public void insertPrioriter(String nomP, String imgP, String desP, String datedebut, String datefin){
        String strSql = "insert into Prioriter (nomP,imgP,desP,datedebut,dateactuelle,datefin) values ('"+nomP+"','"+imgP+"','"+desP+"','"+datedebut+"',date('now'),'"+datefin+"')";
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL(strSql);
        Log.i("DATABASE", "insert invoqué");
    }
    public void DeletePrioriter(String nomP){
        String strSql = "DELETE from Prioriter where nomP = '"+nomP+"' ";
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL(strSql);
        Log.i("DATABASE", "insert invoqué");
    }


}
