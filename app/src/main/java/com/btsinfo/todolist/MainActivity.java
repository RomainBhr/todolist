package com.btsinfo.todolist;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.style.BackgroundColorSpan;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class MainActivity extends AppCompatActivity {

    ArrayList<prioriter>ListePrioriter;
    private DataBaseManager dbm;
    prioriter unePrioriter;

    ListView lvListe;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button bt20 = (Button) findViewById(R.id.button);
        bt20.setOnClickListener(button);
        ListePrioriter = new DataBaseManager(this).lecturePrioriter();

        lvListe = (ListView)findViewById(R.id.listView1);

        lvListe.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int i, long l) {
                startViewActivity(i);
            }
        });


    }

    public View.OnClickListener button = new View.OnClickListener() {

        @Override
        public void onClick(View v) {
            Intent button = new Intent(MainActivity.this, Ajouter_Activit.class);
            startActivity(button);
        }
    };
    @Override
    protected void onResume() {

        super.onResume();
        ListeAdapter listeAdapter = new ListeAdapter(this, ListePrioriter);
        lvListe.setAdapter(listeAdapter);

    }


        private void startViewActivity(int i) {
        prioriter unePrioriter = ListePrioriter.get(i);

        Intent intent = new Intent(this, TacheActivity.class);

        intent.putExtra("idE",unePrioriter.getIdE());
        intent.putExtra("nomP",unePrioriter.getNomP());
        intent.putExtra("imgP",unePrioriter.getImgP());
            intent.putExtra("desP",unePrioriter.getImgP());

        startActivity(intent);

    }
}