package com.btsinfo.todolist;

import android.content.Intent;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import java.io.IOException;
import java.io.InputStream;

public class Ajouter_Activit  extends AppCompatActivity {
    EditText NomTitre, imageTitre, DescTitre, datedebut, datefin;
    Button insert, update, delete, view;
    DataBaseManager dbm;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ajouter_activity);
        NomTitre = findViewById(R.id.NomTitre);
        imageTitre = findViewById(R.id.imageTitre);
        DescTitre = findViewById(R.id.DescTitre);
        datedebut = findViewById(R.id.datedebut);
        datefin = findViewById(R.id.datefin);

        dbm = new DataBaseManager( this );

        insert = findViewById(R.id.btn);

        insert.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String nomTXT = NomTitre.getText().toString();
                String imageTXT = imageTitre.getText().toString();
                String DescTXT = DescTitre.getText().toString();
                String datedebTXT = datedebut.getText().toString();
                String datefinActuelle = datefin.getText().toString();
                String datefinTXT = datefin.getText().toString();
                if(!nomTXT.equals("")){
                    dbm.insertPrioriter(nomTXT, imageTXT, DescTXT, datedebTXT, datefinTXT);
                }else{
                    toastMessage("You must enter a name");
                }
            }

            private void toastMessage(String you_must_enter_a_name) {
            }
        });
    }
}
