package com.btsinfo.todolist;

import android.content.Intent;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import java.io.IOException;
import java.io.InputStream;

public class TacheActivity extends AppCompatActivity {

    TextView tvPersonnage;
    TextView tvDesP;
    TextView tvDesc, NomTitre;
    ImageView imageView;
    private DataBaseManager dbm;
    Button delete;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.tache_main);

        tvPersonnage = (TextView)findViewById(R.id.tvPersonnage);
        tvDesP = (TextView)findViewById(R.id.tvDesP);
        tvDesc = (TextView)findViewById(R.id.tvDesc);
        imageView = (ImageView) findViewById(R.id.imgPersonnage);

        Intent intent = getIntent();
        final String unPersonnage = intent.getStringExtra("nomP");
        String unPersonnage2 = intent.getStringExtra("desP");
        tvPersonnage.setText(unPersonnage);
        tvDesc.setText(unPersonnage2);

        AssetManager manager = this.getAssets();
        InputStream open = null;
        try {
            open = manager.open(intent.getStringExtra("imgP"));
            Bitmap bitmap = BitmapFactory.decodeStream(open);
            imageView.setImageBitmap(bitmap);
        } catch (IOException e) {
            e.printStackTrace();
        }
        dbm = new DataBaseManager( this );
        NomTitre = findViewById(R.id.tvPersonnage);
        delete = findViewById(R.id.sup);

        delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String nomTXT = NomTitre.getText().toString();
                if(!nomTXT.equals("")){
                    dbm.DeletePrioriter(nomTXT);
                }else{
                    toastMessage("You must enter a name");
                }
            }

            private void toastMessage(String you_must_enter_a_name) {
            }
        });
    }


}
