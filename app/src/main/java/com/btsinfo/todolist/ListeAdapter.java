package com.btsinfo.todolist;

import android.content.Context;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Date;

public class ListeAdapter extends ArrayAdapter<prioriter> {
    Context context;

    public ListeAdapter(Context context, ArrayList<prioriter> ListePrioriter){
        super(context, -1,ListePrioriter);
        this.context = context;

    }

    public View getView(int position, View convertView, ViewGroup parent){

        View view;
        prioriter unePrioriter;
        Actions uneAction;
        view=null;
        unePrioriter = getItem(position);

        String test = unePrioriter.getDateActuelle();
        String test1 = unePrioriter.getDatefin();
        Date date = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

        if (convertView==null) {
            LayoutInflater layoutInflater = (LayoutInflater) this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = layoutInflater.inflate(R.layout.ligne, parent, false);

        }else{
            view = convertView;
        }


        // Toast.makeText(this, sdf.format(date.getTime()),Toast.LENGTH_LONG).show();

        Date madate1 = null;
        Date madate2 = null;
        Date critique = null;
        Date urgent = null;
        Date ptiturgent = null;
        Date nonurgent = null;
        try {
            madate1 = sdf.parse(test);
            madate2 = sdf.parse(test1);
            critique = sdf.parse("1970-01-02");
            urgent = sdf.parse("1970-01-03");
            ptiturgent = sdf.parse("1970-01-05");
            nonurgent = sdf.parse("1970-01-06");
        } catch (ParseException e) {
            e.printStackTrace();
        }
        long Critique = critique.getTime();
        long Urgent = urgent.getTime();
        long Ptiturgent = ptiturgent.getTime();
        long Nonurgent = nonurgent.getTime();

        long resultat = madate2.getTime() - madate1.getTime();
        //Toast.makeText(this.context, sdf.format(resultat),Toast.LENGTH_LONG).show();

        TextView tvTitre1 = (TextView)view.findViewById(R.id.Titre1);
        TextView tvTitre2 = (TextView)view.findViewById(R.id.Titre2);
        TextView tvTitre3 = (TextView)view.findViewById(R.id.Titre3);
        TextView tvTitre4 = (TextView)view.findViewById(R.id.Titre4);
        ImageView imageView = (ImageView)view.findViewById(R.id.img1);

        String aujourdhui = unePrioriter.getDatedebut();

        if (resultat >= Critique){
            view.setBackgroundColor(Color.argb(255, 143, 0, 0));
            tvTitre2.setText("Le délai restant est Critique !");
            tvTitre4.setText(unePrioriter.getDescP());
            tvTitre3.setText("Date de début :" + unePrioriter.getDatedebut() + "\n Date de fin : " + unePrioriter.getDatefin());
        }

        if (resultat >= Urgent) {
            view.setBackgroundColor(Color.argb(255, 255, 100, 0));
            tvTitre2.setText("Le délai restant est Très Urgent !");
            tvTitre4.setText(unePrioriter.getDescP());
            tvTitre3.setText("Date de début :" + unePrioriter.getDatedebut() + "\n Date de fin : " + unePrioriter.getDatefin());
        }

        if (resultat >= Ptiturgent)
        {
            view.setBackgroundColor(Color.argb(255, 255, 195, 0));
            tvTitre2.setText("Le délai restant est Urgent !");
            tvTitre4.setText(unePrioriter.getDescP());
            tvTitre3.setText("Date de début :" + unePrioriter.getDatedebut() + "\n Date de fin : " + unePrioriter.getDatefin());
        }
        if (resultat >= Nonurgent)
        {
            view.setBackgroundColor(Color.argb(255, 13, 107, 0));
            tvTitre2.setText("Le délai restant est non urgent !");
            tvTitre4.setText(unePrioriter.getDescP());
            tvTitre3.setText("Date de début :" + unePrioriter.getDatedebut() + "\n Date de fin : " + unePrioriter.getDatefin());
        }
        if (resultat < Critique){
            view.setBackgroundColor(Color.argb(255, 155, 155, 155));
            tvTitre2.setText("Le délai restant a été dépassé !");
            tvTitre4.setText(unePrioriter.getDescP());
            tvTitre3.setText("Date de début :" + unePrioriter.getDatedebut() + "\n Date de fin : " + unePrioriter.getDatefin());
        }

        tvTitre1.setText(unePrioriter.getNomP());

        AssetManager manager = context.getAssets();

        InputStream open = null;

        try {
            open = manager.open(unePrioriter.getImgP());
            Bitmap bitmap = BitmapFactory.decodeStream(open);
            imageView.setImageBitmap(bitmap);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return view;
    }
}

