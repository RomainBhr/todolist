package com.btsinfo.todolist;

public class Actions {
    int IdA;
    int idP;
    String NomA;
    String DesA;
    String DesUn;
    String Datedebut;
    String Datefin;

    public Actions(int idA, int idP, String nomA, String desA, String desUn,String datedebut, String datefin) {
        this.IdA = idA;
        this.idP = idP;
        this.NomA = nomA;
        this.DesA = desA;
        this.DesUn = desUn;
        this.Datedebut = datedebut;
        this.Datefin = datefin;
    }

    public int getIdP() {
        return idP;
    }

    public void setIdP(int idP) {
        this.idP = idP;
    }

    public String getNomA() {
        return NomA;
    }

    public void setNomA(String nomA) {
        NomA = nomA;
    }

    public String getDesUn() {
        return DesUn;
    }

    public void setDesUn(String desUn) {
        DesUn = desUn;
    }

    public String getDesA() {
        return DesA;
    }

    public void setDesA(String desA) {
        DesA = desA;
    }

    public int getCatA() {
        return IdA;
    }

    public void setCatA(int idA) {
        IdA = idA;
    }

    public void setDatedebut(String datedebut) {
        Datedebut = datedebut;
    }

    public String getDatedebut() {
        return Datedebut;
    }

    public String getDatefin() {
        return Datefin;
    }

    public void setDatefin(String datefin) {
        Datefin = datefin;
    }

}
